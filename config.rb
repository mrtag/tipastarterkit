# Require any additional compass plugins here.
require 'compass'
require 'susy'
require 'breakpoint'

# Basic settings.
http_path = "/"
css_dir = "src/css"
sass_dir = "src/styles"
images_dir = "src/img"
javascripts_dir = "src/js"
fonts_dir = "src/fonts"
relative_assets = true
preferred_syntax = :sass
sourcemap = true
line_comments = false
asset_cache_buster :none
output_style = :expanded

Encoding.default_external = 'utf-8'