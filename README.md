## Requirements:
- `Ruby`
- `NodeJS`
- `Bundler`

## What you get?
- Ready to go project structure.
- `BrowserSync` awesomeness.
- Pre-configured `Compass`.
- `Breakpoint` and `Susy` gems on board.
- `Sourcemaps` enabled.
- Powered by `TWIG` template engine.
- `gulp-favicons` enabled and set up.
- JavaScript and CSS minification.
- `JSHint` onboard.
- Images optimization.
- Caching enabled.

## Installation:
1. Install Gulp dependencies

        npm install

2. Install Bundler dependencies

        bundler install

3. Run compass watch

        bundler exec compass watch

4. Start gulp watch or simply build the project

        gulp or gulp build-project

## Utilities:
1. If you want to clear your build directory just run `gulp clear`

2. If you need to clear cache just run `gulp clear-cache`

3. To perform a JavaScript code verification, run `gulp jshint`.

4. To generate favicons make sure your project is built and you have your **default-favicon.png** file located in project root and then just run gulp `favicons-generate`.

  * If you want another file format, or location, you are welcome to edit `favicons-generate` gulp task.

  * Source image must be **square** !

  * Minimal source image size must be **260x260px** !

   For more info visit [gulp-favicons](https://github.com/haydenbleasel/gulp-favicons "gulp-favicons") and [Node.js RealFaviconGenerator](https://github.com/haydenbleasel/favicons "Node.js RealFaviconGenerator").
