/**
 * TODO:
 * 1. Think about enable/disable scripts concatenation.
 * 2. Add scss linters.
 */

(function(){
  'use strict';

  /**
   * Plugins initialization
   */
  var gulp = require('gulp'),
    pckg = require('./package.json'),
    watch = require('gulp-watch'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin'),
    cached = require('gulp-cached'),
    pngquant = require('imagemin-pngquant'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    twig = require('gulp-twig'),
    favicons = require('gulp-favicons'),
    bs = require('browser-sync'),
    bsReload = bs.reload,
    del = require('del');

  /**
   * Flags
   */
  var css_minification = true,
    js_uglify = true;

  /**
   * Main paths
   */
  var path = {
    build: {
      html: 'build',
      js: 'build/js',
      css: 'build/css',
      img: 'build/img',
      fonts: 'build/fonts'
    },
    src: {
      twig: 'src/*.twig',
      js_dir: 'src/js/',
      js_lib_dir: 'src/js/libraries/*.*',
      js_part_dir: 'src/js/partials/*.*',
      js: 'src/js/scripts.js',
      css: 'src/css/main.css',
      source_map: 'src/css/main.css.map',
      img: 'src/img/**/*.*',
      fonts: 'src/fonts/**/*.*'
    },
    watch: {
      twig: 'src/**/*.twig',
      js: 'src/js/**/*.js',
      css: 'src/css/**/*.css',
      img: 'src/img/**/*.*',
      fonts: 'src/fonts/**/*.*'
    },
    clear: {
      build: ['./build', './.sass-cache']
    }
  };

  /**
   * Browser Sync setup
   */
  gulp.task('browser-sync', function() {
    bs({server: {baseDir: path.build.html}});
  });

  /**
   * HTML Build
   */
  gulp.task('build-html', function () {
    gulp.src(path.src.twig)
      .pipe(twig())
      .pipe(gulp.dest(path.build.html))
      .pipe(bsReload({stream: true}));
  });

  /**
   * JS Build
   */
  gulp.task('build-js', function () {
    gulp.src(path.src.js)
      .pipe(rigger())
      .pipe(gulpif(js_uglify, uglify()))
      .pipe(gulp.dest(path.build.js))
      .pipe(bsReload({stream: true}));
  });

  /**
   * JSHint
   */
  gulp.task('jshint', function () {
    gulp.src(path.watch.js)
      .pipe(jshint())
      .pipe(jshint.reporter(stylish));
  });

  /**
   * Copy src js file structure to build folder.
   */
  gulp.task('copy-js', function () {
    gulp.src(path.src.js_lib_dir, { base: path.src.js_dir })
      .pipe(gulp.dest(path.build.js));

    gulp.src(path.src.js_part_dir)
      .pipe(gulp.dest(path.build.js));
  });

  /**
   * CSS Build
   */
  gulp.task('build-css', function () {
    gulp.src(path.src.source_map)
      .pipe(gulp.dest(path.build.css));

    gulp.src(path.src.css)
      .pipe(gulpif(css_minification, cssmin()))
      .pipe(gulp.dest(path.build.css))
      .pipe(bsReload({stream: true}));
  });

  /**
   * Images optimization
   */
  gulp.task('build-images', function () {
    gulp.src(path.src.img)
      .pipe(cached(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()],
        interlaced: true
      })))
      .pipe(gulp.dest(path.build.img))
      .pipe(bsReload({stream: true}));
  });

  /**
   * Move fonts to build directory
   */
  gulp.task('build-fonts', function() {
    gulp.src(path.src.fonts)
      .pipe(gulp.dest(path.build.fonts));
  });

  /**
   * Generates favicons images and right markup.
   *
   * Actually gulp-favicon is just a wrapper for favicons.
   * So all options can be found here:
   * https://github.com/haydenbleasel/favicons
   *
   * TODO
   * Include favicons-generate in project-build task.
   * Create task for clearing favicons files and folders.
   */
   gulp.task('favicons-generate', function () {
    gulp.src('default-favicon.png').pipe(favicons({
      appName: pckg.name,
      appDescription: pckg.description,
      developer: pckg.author,
      background: "#000",
      path: "/img/favicons/",
      online: false,
      version: 1.0,
      logging: true,
      html: "src/blocks/_favicons.twig"
     })).pipe(gulp.dest(build.img + '/favicons/'));
   });

  /**
   * Build the project
   */
  gulp.task('build-project', [
    'build-html',
    'build-js',
    'build-css',
    'build-fonts',
    'build-images'
  ]);

  /**
   * Initialize main watcher
   *
   * Define here all files that must be watched.
   */
  gulp.task('watch', function(){
    watch([path.watch.twig], function(event, cb) {
      gulp.start('build-html');
    });
    watch([path.watch.css], function(event, cb) {
      gulp.start('build-css');
    });
    watch([path.watch.js], function(event, cb) {
      gulp.start('build-js');
    });
    watch([path.watch.img], function(event, cb) {
      gulp.start('build-images');
    });
    watch([path.watch.fonts], function(event, cb) {
      gulp.start('build-fonts');
    });
  });

  /**
   * Removes directories listed in 'path.clean'.
   *
   * Just run 'gulp clear'.
   */
  gulp.task('clear', function (cb) {
    del(path.clear.build, cb);
  });

  /**
   * Clear cache.
   */
  gulp.task('clear-cache', function (cb) {
    cached.caches = {};
  });

  /**
   * Default gulp task.
   */
  gulp.task('default', ['build-project', 'browser-sync', 'watch']);
}());
